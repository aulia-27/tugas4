package com.example.strukturorganisasi.service;

import com.example.strukturorganisasi.model.Employee;
import com.example.strukturorganisasi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    JdbcTemplate jdbc;

    public List<Employee> listAll(){
        return employeeRepository.findAll();
    }

    public List<Employee> listOutput(){
        List<Employee> employees = jdbc.query(
                "SELECT e.id, e.nama, if(em.nama=e.nama,'CEO', em.nama) as atasan, c.nama from employee e " +
                        "join employee em join company c on e.company_id = c.id " +
                        "where e.atasan_id = em.id or e.atasan_id is null group by e.id" ,
                (rs, rowNum) -> new Employee(rs.getInt("e.id"), rs.getString("e.nama"), rs.getString("atasan"),rs.getString("c.nama")));
        return employees;
    }

    public void save(Employee employee){
        employeeRepository.save(employee);
    }

    public Employee get(int id){
        return employeeRepository.findById(id).get();
    }

    public void delete(int id){
        employeeRepository.deleteById(id);
    }


}
