package com.example.strukturorganisasi.service;

import com.example.strukturorganisasi.model.Company;
import com.example.strukturorganisasi.model.Employee;
import com.example.strukturorganisasi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> listAll(){
        return companyRepository.findAll();
    }


    public void save(Company company){
        companyRepository.save(company);
    }

    public Company get(int id){
        return companyRepository.findById(id).get();
    }

    public void delete(int id){
        companyRepository.deleteById(id);
    }
}
